class InfosController < ApplicationController
  before_action :set_info, only: [:show, :edit, :update, :destroy]
  before_action :impedeNaoAdmin, except: [:getInfo, :quemSomos]

  # GET /infos
  # GET /infos.json
  def index
    @infos = Info.all
  end

  # GET /infos/1
  # GET /infos/1.json
  def show
  end

  def getInfo
    @info = Info.last
  end

  # GET /infos/new
  def new
    @info = Info.new
  end

  # GET /infos/1/edit
  def edit
    if Info.find(1).nil?
      firstInfo = Info.new
      firstInfo.who = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non diam placerat, rutrum felis vel, ullamcorper ligula. Praesent eu finibus enim. Sed odio felis, feugiat quis pretium vel, tincidunt quis ante. Donec risus est, varius nec pharetra tristique, semper eu eros. Duis quis ipsum hendrerit, pharetra mi vitae, euismod neque. Cras vitae sapien vel nisi pharetra varius at non tellus. Maecenas eleifend elit neque, eu fermentum arcu vulputate non. Integer egestas placerat arcu, sed condimentum erat convallis suscipit. '
      firstInfo.email = 'pedepizza@contato.com'
      firstInfo.phone = '99999-9999'
      firstInfo.save
    end
  end

  # POST /infos
  # POST /infos.json
  def create
    @info = Info.new(info_params)

    respond_to do |format|
      if @info.save
        format.html { redirect_to @info, notice: 'Info was successfully created.' }
        format.json { render :show, status: :created, location: @info }
      else
        format.html { render :new }
        format.json { render json: @info.errors, status: :unprocessable_entity }
      end
    end
  end

  def quemSomos
    if Info.last.nil?
      firstInfo = Info.new
      firstInfo.who = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque non diam placerat, rutrum felis vel, ullamcorper ligula. Praesent eu finibus enim. Sed odio felis, feugiat quis pretium vel, tincidunt quis ante. Donec risus est, varius nec pharetra tristique, semper eu eros. Duis quis ipsum hendrerit, pharetra mi vitae, euismod neque. Cras vitae sapien vel nisi pharetra varius at non tellus. Maecenas eleifend elit neque, eu fermentum arcu vulputate non. Integer egestas placerat arcu, sed condimentum erat convallis suscipit. '
      firstInfo.email = 'pedepizza@contato.com'
      firstInfo.phone = '99999-9999'
      firstInfo.save
    end
    @infos = Info.last
  end

  # PATCH/PUT /infos/1
  # PATCH/PUT /infos/1.json
  def update
    respond_to do |format|
      if @info.update(info_params)
        format.html { redirect_to @info, notice: 'Info was successfully updated.' }
        format.json { render :show, status: :ok, location: @info }
      else
        format.html { render :edit }
        format.json { render json: @info.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /infos/1
  # DELETE /infos/1.json
  def destroy
    @info.destroy
    respond_to do |format|
      format.html { redirect_to infos_url, notice: 'Informações adicionadas com sucesso!' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_info
      @info = Info.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def info_params
      params.require(:info).permit(:who, :email, :phone)
    end

    def impedeNaoAdmin
      if !current_user.admin
        redirect_to root_path
      end
    end
end
