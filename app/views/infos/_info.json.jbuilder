json.extract! info, :id, :who, :email, :phone, :created_at, :updated_at
json.url info_url(info, format: :json)
