json.extract! promocao, :id, :price, :produto_id, :created_at, :updated_at
json.url promocao_url(promocao, format: :json)
