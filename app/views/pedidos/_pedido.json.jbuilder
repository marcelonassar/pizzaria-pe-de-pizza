json.extract! pedido, :id, :payment, :status, :produtos_id, :address_id, :created_at, :updated_at
json.url pedido_url(pedido, format: :json)
