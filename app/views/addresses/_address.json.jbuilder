json.extract! address, :id, :cep, :street, :number, :complement, :created_at, :updated_at
json.url address_url(address, format: :json)
