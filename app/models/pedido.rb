class Pedido < ApplicationRecord
  has_one :car
  belongs_to :user, optional: true
end
