require "application_system_test_case"

class PromocaosTest < ApplicationSystemTestCase
  setup do
    @promocao = promocaos(:one)
  end

  test "visiting the index" do
    visit promocaos_url
    assert_selector "h1", text: "Promocaos"
  end

  test "creating a Promocao" do
    visit promocaos_url
    click_on "New Promocao"

    fill_in "Price", with: @promocao.price
    fill_in "Produto", with: @promocao.produto_id
    click_on "Create Promocao"

    assert_text "Promocao was successfully created"
    click_on "Back"
  end

  test "updating a Promocao" do
    visit promocaos_url
    click_on "Edit", match: :first

    fill_in "Price", with: @promocao.price
    fill_in "Produto", with: @promocao.produto_id
    click_on "Update Promocao"

    assert_text "Promocao was successfully updated"
    click_on "Back"
  end

  test "destroying a Promocao" do
    visit promocaos_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Promocao was successfully destroyed"
  end
end
