Rails.application.routes.draw do
  resources :contatos
  resources :infos
  resources :promocaos
  resources :users
  resources :pedidos
  resources :addresses
  resources :produtos

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  root 'static_pages#home'

  get '/contato' , to: 'contatos#new'

  get '/login' , to: 'sessions#new'
  post '/login' , to: 'sessions#create'

  delete '/logout', to: 'sessions#destroy'

  get '/showmenu', to: 'produtos#showMenu'

  post '/addtocar', to: 'produtos#addToCar'

  post '/updateStatus', to: 'pedidos#updateStatus'

  get '/quemsomos' , to: 'infos#quemSomos'
end
