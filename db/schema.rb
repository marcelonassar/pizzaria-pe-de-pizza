# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_02_01_172710) do

  create_table "addresses", force: :cascade do |t|
    t.integer "cep"
    t.string "street"
    t.integer "number"
    t.text "complement"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "car_products", force: :cascade do |t|
    t.integer "car_id"
    t.integer "produto_id"
    t.integer "quantity"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["car_id"], name: "index_car_products_on_car_id"
    t.index ["produto_id"], name: "index_car_products_on_produto_id"
  end

  create_table "cars", force: :cascade do |t|
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_cars_on_user_id"
  end

  create_table "contatos", force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.text "message"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "infos", force: :cascade do |t|
    t.text "who"
    t.string "email"
    t.string "phone"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "pedidos", force: :cascade do |t|
    t.string "payment"
    t.string "status"
    t.string "address"
    t.integer "users_id"
    t.integer "cars_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["cars_id"], name: "index_pedidos_on_cars_id"
    t.index ["users_id"], name: "index_pedidos_on_users_id"
  end

  create_table "produtos", force: :cascade do |t|
    t.text "name"
    t.text "description"
    t.float "price"
    t.integer "promocaos_id"
    t.integer "pedidos_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["pedidos_id"], name: "index_produtos_on_pedidos_id"
    t.index ["promocaos_id"], name: "index_produtos_on_promocaos_id"
  end

  create_table "promocaos", force: :cascade do |t|
    t.float "price"
    t.integer "info_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["info_id"], name: "index_promocaos_on_info_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.string "password_digest"
    t.boolean "admin"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
