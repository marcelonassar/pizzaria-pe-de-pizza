class CreatePromocaos < ActiveRecord::Migration[5.2]
  def change
    create_table :promocaos do |t|
      t.float :price
      t.references :info, foreign_key: true

      t.timestamps
    end
  end
end
