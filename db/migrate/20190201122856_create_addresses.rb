class CreateAddresses < ActiveRecord::Migration[5.2]
  def change
    create_table :addresses do |t|
      t.integer :cep
      t.string :street
      t.integer :number
      t.text :complement

      t.timestamps
    end
  end
end
