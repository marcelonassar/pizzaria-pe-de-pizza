class CreateCarProducts < ActiveRecord::Migration[5.2]
  def change
    create_table :car_products do |t|
      t.references :car, foreign_key: true
      t.references :produto, foreign_key: true
      t.integer :quantity

      t.timestamps
    end
  end
end
