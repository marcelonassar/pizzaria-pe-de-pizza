class CreatePedidos < ActiveRecord::Migration[5.2]
  def change
    create_table :pedidos do |t|
      t.string :payment
      t.string :status
      t.string :address
      t.references :users, foreign_key: true
      t.references :cars, foreign_key: true
      t.timestamps
    end
  end
end
